package de.fraunhofer.fokus.asct.behave.simulation.main;
import com.dcaiti.phabmacs.sim.Simulator;
import com.dcaiti.phabmacs.sim.scenario.controllers.drive.DriveController;
import com.dcaiti.phabmacs.sim.scenario.controllers.lowlevel.PhabmacsLowLevelController;
import com.dcaiti.phabmacs.sim.scenario.controllers.lowlevel.RegulatedCarProperties;
import com.dcaiti.phabmacs.sim.vehicle.Vehicle;
import com.dcaiti.phabmacs.sim.vehicle.driver.DefaultDriver;

public class CustomDriver extends DefaultDriver {
    private RegulatedCarProperties rcp;
    private PhabmacsLowLevelController phabmacsLowLevelController = null;


    @Override
    public void makeDecisions(double deltaT) {
        super.makeDecisions(deltaT);
        if (Simulator.getInstance().getSimulationTime() < 1) {
            System.out.println("if Statement: " + System.currentTimeMillis());
            rcp.setDesiredSpeed(0, DriveController.DriveControllerPriority.MANDATORY);
            return;
        }

        //Change the speed of the car according to action which is taken from the agent and passed through the zmq
        double curr_speed = vehicle.getSpeed();
        double acc_action = ZMQMessageProcessor.getAction() + curr_speed ;
        System.out.println("before setDesiredSpeed " + System.currentTimeMillis());
        rcp.setDesiredSpeed(acc_action, DriveController.DriveControllerPriority.MANDATORY);
        System.out.println("after setDesiredSpeed " + System.currentTimeMillis());
        phabmacsLowLevelController.control(rcp, deltaT);
        System.out.println("after phabmacsLowLevelController " + System.currentTimeMillis());
        rcp.setDesiredSpeed(3, DriveController.DriveControllerPriority.MANDATORY);
        phabmacsLowLevelController.control(rcp, deltaT);
    }


    @Override
    public void attach(Vehicle vehicle) {
        super.attach(vehicle);

        //set controller
        rcp = new RegulatedCarProperties(vehicle);
        phabmacsLowLevelController = PhabmacsLowLevelController.createFactory(vehicle).create();
        phabmacsLowLevelController.setVehicle(vehicle);

    }
}
