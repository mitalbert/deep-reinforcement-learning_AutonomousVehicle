package de.fraunhofer.fokus.asct.behave.simulation.main;

import com.dcaiti.phabmacs.sim.GeoLocation;
import com.dcaiti.phabmacs.sim.Simulator;
import com.dcaiti.phabmacs.sim.map.Map;
import com.dcaiti.phabmacs.sim.map.OsmMap;
import com.dcaiti.phabmacs.sim.math.Vector3d;
import com.dcaiti.phabmacs.sim.vehicle.Car;
import com.dcaiti.phabmacs.sim.vehicle.Vehicle;
import com.dcaiti.phabmacs.sim.vehicle.driver.DefaultDriver;
import com.dcaiti.phabmacs.sim.vehicle.sensors.GnssSensor;
import com.dcaiti.phabmacs.sim.vehicle.sensors.LaneDetectionSensor;
import com.dcaiti.phabmacs.sim.vehicle.sensors.LaserScanner;
import com.dcaiti.phabmacs.sim.vehicle.sensors.OutOfLaneSensor;
import com.dcaiti.phabmacs.visualizer.PhabmacsGlContext;
import com.dcaiti.phabmacs.visualizer.Visualizer;
import com.dcaiti.phabmacs.visualizer.lightgl.util.Color;
import de.fraunhofer.fokus.asct.behave.simulation.main.CustomDriver;
import de.fraunhofer.fokus.asct.flcon.messageformat.geo.Lanes;


public class Main {
    public static final Vehicle vehicle = new Car("/models/e-class.properties");
    //public static final Vehicle vehicle = new Car("/models/smart.properties");
    public static final Vector3d STARTPOINT = new Vector3d(60,0.00,443);
    //Simulator sim = Simulator.getInstance();
    //public static final Vector3d STARTPOINT = new Vector3d(-200,0.00,-1052);

    public static void main(String[] args) {
        Simulator sim = Simulator.getInstance();
        Simulator.getInstance().setReportGroundCollisions(false);

        OsmMap map = new OsmMap("maps/ernst-reuter-platz_large.osm");
        //map.setHasCollision(false);
        map.buildMap();
        sim.setMap(map);
        sim.setCheckCollisions(false);

        //create visualizer
        Visualizer visualizer = new Visualizer();

        vehicle.setDriver(new CustomDriver());

        vehicle.setPosition(STARTPOINT, -95);
        sim.addSimulationObject(vehicle);
        vehicle.setColor(Color.MAT_CYAN_300);

        vehicle.getSensorManager().addSensor(new LaserScanner());
        vehicle.getSensorManager().addSensor(new LaneDetectionSensor());
        vehicle.getSensorManager().addSensor(new GnssSensor());

        visualizer.setTrackedVehicle(vehicle);
        vehicle.getDriver().makeDecisions(2.0);

        ZMQServer server = new ZMQServer();
        server.setProcessor(new ZMQMessageProcessor(vehicle));
        try {
            Thread thread = new Thread(server, "New Thread");
            thread.start();
            System.out.println("Thread created");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        //GeoLocation test = sim.toGeoCoords(this.vehicle.getPosition(new Vector3d()), new GeoLocation());

        //Manuel snippet
        double startTime = sim.getSimulationTime();
        long startSysTime = System.currentTimeMillis();
        sim.startSimulationLoop(Simulator.SIM_TIME_INFINITE, true);
        while (true) { //make break condition, otherwise you'D have to kill the precess
            sim.stepSimulation(1.0/60.0);
            double elapsedTime = (sim.getSimulationTime() - startTime) * 1000;
            double elapsedRealTime = System.currentTimeMillis() - startSysTime;
            if (elapsedRealTime < elapsedTime) {
                try {
                    Thread.sleep(Math.round(elapsedTime - elapsedRealTime));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        //end
    }
}