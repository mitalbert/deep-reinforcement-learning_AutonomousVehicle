package de.fraunhofer.fokus.asct.behave.simulation.main;

import com.dcaiti.phabmacs.sim.Simulator;
import com.dcaiti.phabmacs.sim.vehicle.Vehicle;
import static de.fraunhofer.fokus.asct.behave.simulation.main.Main.STARTPOINT;

/*
We have used this code to train the car to drive at a desired speed as well for staying on the lane using steering,
so everything is commented is for steering version.
It is irrelevant for desired speed (submission version) but we have left it because it is useful for our planned future work.
And in case you are interested in seeing how far did we get with this version.
*/

public class ZMQMessageProcessor {

    private final String OBSERVATIONREQ = "obs";
    private final String RESREQ = "res";
    //private final String TURNLEFT = "left";
    //private final String TURNRIGHT = "right";
    //private final String GOSTRAIGHT = "straight";
    //private final String THROTTLE = "throttle";
    private final String DECREASEALITTLE = "declit";
    private final String DECREASEALOT = "declot";
    private final String INCREASEALOT = "inclot";
    private final String INCREASEALITLLE = "inclit";
    private final String DONTH = "nth";

    protected Vehicle vehicle;
    static double action = 0;

    public ZMQMessageProcessor(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    String handleMessage(byte[] request) {
        String msgType = new String(request);
        String rsp = null;
        //double curr_SteeringAngle = 0;
        //curr_SteeringAngle =vehicle.getSteerAngle();
        switch (msgType) {
            case OBSERVATIONREQ:
                //do nth just send the env data
                break;
            case DECREASEALITTLE:
                action = -0.27;
                System.out.println("DECREASEALITTLE");
                break;
            case RESREQ:
                rsp = processResetReq();
                break;
            case DECREASEALOT:
                action = -1.38;
                System.out.println("DECREASEALOT");
                break;
            case INCREASEALOT:
                action = 1.38;
                System.out.println("INCREASEALOT");
                break;
            case INCREASEALITLLE:
                action = 0.27;
                System.out.println("INCREASEALITLLE");
                break;
            case DONTH:
                action = 0;
                System.out.println("do nth");
                break;
             /*
             //Steering Version
            case TURNLEFT:
                System.out.println("turn left");
                vehicle.setSteerAngle(0.1);
                //vehicle.setSteerAngle(curr_SteeringAngle+0.03);
                break;
            case TURNRIGHT:
                System.out.println("turn right");
                vehicle.setSteerAngle(-0.1);
                //vehicle.setSteerAngle(curr_SteeringAngle-0.03);
                break;
            case GOSTRAIGHT:
                System.out.println("straight");
                vehicle.setSteerAngle(0);
                break;
             */
            default:
                break;
        }
        rsp = processObservationReq();
        return rsp;
    }

    String processObservationReq() {
        String res = "";
        /*
        //Steering version
        LaneMeasurement laneMeasurement  =  vehicle.getSensorManager().getSensor(LaneDetectionSensor.class).getLatestMeasurement();
        int direction;
        Simulator sim = Simulator.getInstance();
        try {
            double dis_center = laneMeasurement.getDistanceToLaneCenter();
            double dis_left = laneMeasurement.getDistanceToLaneBorderLeft();
            double dis_right = laneMeasurement.getDistanceToLaneBorderRight();
            if(dis_left > dis_right){
                direction = 1;
            }else{
                direction = 0;
            }
            //res = dis_center + "," + dis_right + "," + dis_left;
            res = dis_center + ", " + direction;
            //GeoLocation test = sim.toGeoCoords(this.vehicle.getPosition(new Vector3d()), new GeoLocation());
            //System.out.println("Location: " +  test);
            //System.out.println(vehicle.getPosition();
        }catch(Exception e){
            System.out.println(e.getMessage());
            res = ("10,0");
        }
        */

        double speed = vehicle.getSpeed();
        res = speed + " " ;
        return res;
    }

    String processResetReq(){
        vehicle.setPosition(STARTPOINT, -95);
        String rsp = processObservationReq();
        return rsp;
    }

    static double getAction(){
        return action;
    }

}