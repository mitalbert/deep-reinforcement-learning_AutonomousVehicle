package de.fraunhofer.fokus.asct.behave.simulation.main;
import com.dcaiti.phabmacs.sim.vehicle.sensors.LaserScanner;
import org.zeromq.ZMQ;
import com.dcaiti.phabmacs.sim.vehicle.Vehicle;

public class ZMQServer implements Runnable {
    private ZMQMessageProcessor processor;

    public ZMQServer() {
    }

    void setProcessor(ZMQMessageProcessor processor){
        this.processor = processor;
    }
    @Override
    public void run() {
        ZMQ.Context context = ZMQ.context(1);

        //  Socket to talk to clients
        ZMQ.Socket responder = context.socket(ZMQ.REP);
        responder.bind("tcp://127.0.0.1:5555");
        System.out.println("ZMQServer Run()");

        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(100);
            }catch (Exception e){

            }
            // Wait for next request from the client
            byte[] request = responder.recv(0);
            //System.out.println("Received " + new String (request));
            String reply = processor.handleMessage(request);


            // Send reply back to client
            responder.send(reply.getBytes(), 0);
        }
        responder.close();
        context.term();
    }
}
